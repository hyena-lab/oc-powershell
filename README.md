Vue d'ensemble

Cette application vous permet de faire des recherches par groupes et par utilisateurs pour effectuer des tâches qui ne sont pas entièrement rendues faciles par Active Directory.
    Comme par exemple :
        Obtenir des informations sur les utilisateurs
        Copier une sélection d'utilisateurs vers un autre groupe
        Copier un groupe entier d'utilisateurs vers un autre groupe
        Localisation de la machine qui a bloqué un utilisateur
        Affichage de tous les utilisateurs dans un groupe
        Affichage de tous les groupes auxquels un utilisateur appartient
    Collez le code dans Powershell ISE et appuyez sur Play

Info

    Les autorisations sont basées sur les permissions de l'utilisateur qui exécute l'application.
    Un fichier journal est créé/mis à jour dans C:\Temp\ADTool\Log-DATE.txt
    L'application vérifie d'abord si vous avez une connexion à Active Directory.
    La plupart des applications fonctionnent avec les autorisations en lecture seule de l'AD
    Pour ajouter des utilisateurs à des groupes, vous devez avoir l'autorisation de le faire.
    La recherche de l'origine d'un utilisateur bloqué, cela nécessitera des autorisations d'administrateur de domaine, car     cela implique la recherche dans les journaux d'événements de sécurité d'un contrôleur de domaine.
    L'aide peut être affichée avec le bouton en haut à droite.
    La recherche d'utilisateurs et de groupes ne nécessite pas le nom complet. Le domaine A retournera toujours Admins de domaine
    L'application permet de tabuler autour des attributs du formulaire et d'appuyer sur la touche Entrée dans les zones de recherche et lorsque les boutons sont mis en évidence.
    Lors de la recherche d'un utilisateur, le caractère final est ignoré, ceci afin de permettre la recherche d'utilisateurs par nom de famille et nom d'utilisateur.

Fonctions

    Recherche du groupe source
        Saisissez un terme de recherche pour un ou plusieurs groupes dans la boîte de recherche ci-dessus et appuyez sur Entrée/Search Source Group.
        La liste de gauche s'affichera alors avec les résultats pertinents.
        Vous ne pouvez mettre en évidence qu'un seul de ces résultats à la fois.

    Groupe de destination de la recherche
        Saisissez un terme de recherche pour un ou plusieurs groupes dans la zone de recherche ci-dessus et appuyez sur Entrée/Groupe de destination de la recherche.
        La boîte de liste de droite s'affichera alors avec les résultats pertinents.
        Vous ne pouvez mettre en évidence qu'un seul de ces résultats à la fois.

    Recherche par nom
        Saisissez un terme de recherche pour un ou plusieurs utilisateurs dans la zone de recherche ci-dessus et appuyez sur   Entrée/Recherche par nom.
        La liste de gauche s'affichera alors avec les résultats pertinents.
        Vous pouvez mettre en évidence plusieurs utilisateurs à la fois.

    Informations sur l'utilisateur
        Recherchez un utilisateur dans la zone de recherche du nom d'utilisateur.
        Mettez en évidence un utilisateur dans la zone de liste et sélectionnez Informations sur l'utilisateur pour remplir la zone inférieure avec les attributs AD de cet utilisateur.

    Copie sélectionnée
        Lorsque vous recherchez des utilisateurs en utilisant des caractères minimaux, vous êtes susceptible de recevoir plus d'un résultat.
        Cette fonction vous permet de mettre en évidence plusieurs de ces utilisateurs et de les copier dans un groupe mis en évidence dans la zone de liste de droite.
        Il vous sera demandé de confirmer l'action.

    Afficher les utilisateurs
        Cette fonction vous permet de lister tous les utilisateurs d'un groupe.
        Recherchez un groupe source afin que les résultats soient placés dans la liste de gauche.
        Mettez en évidence un des résultats et sélectionnez Afficher les utilisateurs pour remplir à nouveau la case avec les utilisateurs de ce groupe.

    Adhésions
        Cette fonction vous permet de lister tous les groupes auxquels un utilisateur appartient.
        Recherchez un utilisateur à l'aide de la boîte de recherche et mettez en évidence l'un des utilisateurs.
        Sélectionnez Adhésions pour remplir la case de droite avec les groupes auxquels l'utilisateur appartient.

    Copier tous les utilisateurs
        Recherchez à la fois un groupe de source et un groupe de destination pour que les boîtes soient remplies.
        Mettez en évidence un groupe de chaque boîte et sélectionnez Copier tous les utilisateurs
        Il vous sera demandé de confirmer avant d'ajouter au groupe de destination tous les utilisateurs contenus dans le groupe source.
        Ce faisant, aucun utilisateur n'est supprimé du groupe source.

    Afficher le verrouillage
        Cette fonction suppose de vouloir localiser la machine qui bloque un utilisateur.
        Recherchez un utilisateur et mettez son nom en surbrillance.
        Sélectionnez Afficher le verrouillage pour remplir la boîte verbeuse du bas avec les résultats de la recherche.
        Cela nécessite des autorisations d'administrateur de domaine.

    aide
        Sélectionnez l'aide pour recevoir un résumé de ces instructions dans le cadre verbeux du bas.

Dépannage

    Si la première vérification échoue et qu'on vous dit que vous n'avez pas de connexion à Active Directory - veuillez vous en assurer :
        Vous êtes connecté au réseau
        Vous avez installé Active Directory Services
        Vous avez le module Powershell pour les services AD installé et disponible.
            Pour le tester : ouvrez powerhell et essayez Import-Module Active Directory
            Une erreur implique que vous n'avez pas installé le module ou qu'il fonctionne mal.
    Si vous ne pouvez pas copier des membres entre les groupes, veuillez vous assurer que vous utilisez l'application en tant qu'utilisateur ayant la permission de le faire.
    Ce qui précède s'applique également au scénario dans lequel vous ne pouvez pas afficher l'emplacement du verrouillage
    Si votre recherche de nom d'utilisateur ne donne rien, essayez d'enlever le dernier ou les deux derniers caractères de votre recherche et faites une recherche parmi les résultats.
