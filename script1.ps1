$UserList = Import-Csv -Path 'C:\ps1\user list.csv'

foreach ($User in $UserList) {

     $Attributes = @{

        Enabled = $true
        ChangePasswordAtLogon = $true
        Path = "OU=acme Users,DC=acme,DC=local"

        Name = "$($User.First) $($User.Last)"
        UserPrincipalName = "$($User.First).$($User.Last)@acme.local"
        SamAccountName = "$($User.First).$($User.Last)"

        GivenName = $User.First
        Surname = $User.Last

        Company = $User.Company
        Department = $User.Department
        Title = $User.Title
        AccountPassword = "Password123" | ConvertTo-SecureString -AsPlainText -Force

     }

    New-ADUser @Attributes
